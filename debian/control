Source: tryton-health-meta
Maintainer: Debian Tryton Maintainers <team+tryton-team@tracker.debian.org>
Uploaders: Mathias Behrle <mathiasb@m9s.biz>
Section: metapackages
Priority: optional
Build-Depends: debhelper (>= 9)
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/tryton-team/tryton-health-meta
Vcs-Git: https://salsa.debian.org/tryton-team/tryton-health-meta.git
Homepage: http://www.tryton.org/

Package: tryton-modules-health-all
Architecture: all
Depends: tryton-modules-health (>= ${version:major}),
         tryton-modules-health-archives (>= ${version:major}),
         tryton-modules-health-calendar (>= ${version:major}),
         tryton-modules-health-crypto (>= ${version:major}),
         tryton-modules-health-disability (>= ${version:major}),
         tryton-modules-health-genetics (>= ${version:major}),
         tryton-modules-health-gyneco (>= ${version:major}),
         tryton-modules-health-history (>= ${version:major}),
         tryton-modules-health-icd9procs (>= ${version:major}),
         tryton-modules-health-icd10 (>= ${version:major}),
         tryton-modules-health-icd10pcs (>= ${version:major}),
         tryton-modules-health-icpm (>= ${version:major}),
         tryton-modules-health-icu (>= ${version:major}),
         tryton-modules-health-imaging (>= ${version:major}),
         tryton-modules-health-inpatient (>= ${version:major}),
         tryton-modules-health-inpatient-calendar (>= ${version:major}),
         tryton-modules-health-iss (>= ${version:major}),
         tryton-modules-health-lab (>= ${version:major}),
         tryton-modules-health-lifestyle (>= ${version:major}),
         tryton-modules-health-mdg6 (>= ${version:major}),
         tryton-modules-health-ntd (>= ${version:major}),
         tryton-modules-health-ntd-chagas (>= ${version:major}),
         tryton-modules-health-ntd-dengue (>= ${version:major}),
         tryton-modules-health-nursing (>= ${version:major}),
         tryton-modules-health-ophthalmology (>= ${version:major}),
         tryton-modules-health-pediatrics (>= ${version:major}),
         tryton-modules-health-pediatrics-growth-charts (>= ${version:major}),
         tryton-modules-health-pediatrics-growth-charts-who (>= ${version:major}),
         tryton-modules-health-profile (>= ${version:major}),
         tryton-modules-health-qrcodes (>= ${version:major}),
         tryton-modules-health-reporting (>= ${version:major}),
         tryton-modules-health-services (>= ${version:major}),
         tryton-modules-health-socioeconomics (>= ${version:major}),
         tryton-modules-health-stock (>= ${version:major}),
         tryton-modules-health-surgery (>= ${version:major}),
         tryton-modules-health-who-essential-medicines (>= ${version:major}),
         ${misc:Depends}
Description: Tryton Application Platform (GNU Health Modules Metapackage)
 Tryton is a high-level general purpose application platform. It is the base
 of a complete business solution as well as a comprehensive health and hospital
 information system (GNUHealth).
 .
 This package is a metapackage depending on all available Tryton GNUHealth
 modules.
